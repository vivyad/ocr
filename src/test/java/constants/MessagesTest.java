package constants;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.junit.jupiter.api.Test;

class MessagesTest {

	@Test
	void test() throws NoSuchMethodException, SecurityException, InstantiationException, InvocationTargetException {

		final InvocationTargetException expectedCause = new InvocationTargetException(
				new IllegalAccessException(Messages.PRIVATE_CONS_MSG));
		// Accessing private constructor
		final Constructor<Messages> constructor = Messages.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		try {
			constructor.newInstance();
		} catch (final InvocationTargetException | IllegalAccessException | IllegalArgumentException ex) {
			assertEquals(expectedCause.getClass(), ex.getClass(), "Exeception caused by the exepected cause.");
			assertEquals(expectedCause.getMessage(), ex.getMessage(),
					"Exeception message and exepected cause message should match.");
		}
	}

}
