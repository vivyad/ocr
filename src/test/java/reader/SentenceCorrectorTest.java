package reader;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import constants.Messages;

class SentenceCorrectorTest {

	@Test
	void test() throws NoSuchMethodException, SecurityException, InstantiationException, InvocationTargetException {

		final InvocationTargetException expectedCause = new InvocationTargetException(
				new IllegalAccessException(Messages.PRIVATE_CONS_MSG));
		// Accessing private constructor
		final Constructor<SentenceCorrector> constructor = SentenceCorrector.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		try {
			constructor.newInstance();
		} catch (final InvocationTargetException | IllegalAccessException | IllegalArgumentException ex) {
			assertEquals(expectedCause.getClass(), ex.getClass(), "Exeception caused by the exepected cause.");
			assertEquals(expectedCause.getMessage(), ex.getMessage(),
					"Exeception message and exepected cause message should match.");
		}
	}

	@Test
	void testGetMostFeasibleSequenceString() throws IOException {
		final String[] input = { "|", "am", "curious", "about", "area", "filling" };
		String[] result = SentenceCorrector.getMostFeasibleSequence(input);
		assertEquals(Arrays.asList("i", "am", "curious", "about", "area", "filling"), Arrays.asList(result),
				"The result should match with the expected values.");
	}

	@Test
	void testGetMostFeasibleSequenceStringString() throws IOException {
		final String[] input = { "then", "!!!!", "blah", "blue" };
		String[] result = SentenceCorrector.getMostFeasibleSequence(input, "text/sample.txt");
		assertEquals(Arrays.asList("then", "this", "fear", "blue"), Arrays.asList(result),
				"The result should match with the expected values.");
	}

	@Test
	void testGetMostFeasibleSequenceStringStringManipulated() throws IOException {
		final String[] input = { "then", "!!!!", "blah", "blue" };
		String[] result = SentenceCorrector.getMostFeasibleSequence(input, "text/sample_manipulated.txt");
		assertEquals(Arrays.asList("then", "this", "talk", "blue"), Arrays.asList(result),
				"The result should match with the expected values.");
	}

}
