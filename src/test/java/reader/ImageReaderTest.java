package reader;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.Test;

class ImageReaderTest {

	@Test
	void testExtractImageFile() {
		final String imgPath = "src/main/resources/img/EnglishText.png";
		final String outputTextFromImage = ImageReader.extractImage(new File(imgPath), "eng").trim();
		final String expected = "I am curious about\narea-filling text\nrendering options";
		assertEquals(expected, outputTextFromImage, "OCR output should match with the expected text.");
	}

	@Test
	void testExtractImageFileError() {
		final String imgPath = "src/main/resources/img/na_available.png";
		final String outputTextFromImage = ImageReader.extractImage(new File(imgPath), "eng").trim();
		final String expected = "Error while reading image\njava.lang.IllegalStateException: No input source set!";
		assertEquals(expected, outputTextFromImage, "Error message should match with the expected error message.");
	}

}
