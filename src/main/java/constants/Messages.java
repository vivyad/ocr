package constants;

import java.lang.reflect.InvocationTargetException;

public class Messages {

	// Inaccessible
	private Messages() throws InvocationTargetException {
		throw new InvocationTargetException(new IllegalAccessException(Messages.PRIVATE_CONS_MSG));
	}

	public static final String PRIVATE_CONS_MSG = "Class cannot be instantiated.";

}
