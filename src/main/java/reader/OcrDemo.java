package reader;

import java.io.File;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OcrDemo {

    private static final Logger log = LoggerFactory.getLogger(OcrDemo.class);

	public static void main(String[] args) throws Exception {
		OcrDemo.goodImage();
		OcrDemo.vagueImage();
	}

	private static void goodImage() {
		log.info("OCR for Good Image Started...");

		final String imgPath = "src/main/resources/img/EnglishText.png";
		final String outputTextFromImage = ImageReader.extractImage(new File(imgPath), "eng");
		log.info("Output:\n{}", outputTextFromImage);
		if (outputTextFromImage.contains(ImageReader.READING_ERROR)) {
			System.exit(1);
		}
	}

	// Word correction is conducted for image below
	private static void vagueImage() throws Exception {
		log.info("OCR for Vague Image Started...");

		final String imgPath = "src/main/resources/img/EnglishText_vague.png";
		final String outputTextFromImage = ImageReader.extractImage(new File(imgPath), "eng");
		log.info("Output:\n{}", outputTextFromImage);
		if (outputTextFromImage.contains(ImageReader.READING_ERROR)) {
			System.exit(1);
		}
		// Correct Output Text
		final String[] tokensFromOuput = outputTextFromImage.replace("-", " ").split(" |\\\n");
		log.info("Tokens:\n{}", Arrays.asList(tokensFromOuput));
		final String[] formattedToken = SentenceCorrector.getMostFeasibleSequence(tokensFromOuput);
		log.info("Formatted Tokens:\n{}", Arrays.asList(formattedToken));
	}

}
