package reader;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import constants.Messages;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class ImageReader {

	private static final Logger log = LoggerFactory.getLogger(ImageReader.class);
	
	public static final String READING_ERROR = "Error while reading image";

	// Inaccessible
	private ImageReader() throws InvocationTargetException {
		throw new InvocationTargetException(new IllegalAccessException(Messages.PRIVATE_CONS_MSG));
	}

	public static String extractImage(File imageFile, String language) {
		ITesseract instance = new Tesseract();
		try {
			instance.setLanguage(language);
			return instance.doOCR(imageFile);
		} catch (TesseractException e) {
			final String message = String.format("%s%n%s", ImageReader.READING_ERROR, e.getMessage());
			log.error(message);
			return message;
		}
	}
}