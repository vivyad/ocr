package reader;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import constants.Messages;

public class SentenceCorrector {

	private static final Logger log = LoggerFactory.getLogger(SentenceCorrector.class);

	// Inaccessible
	private SentenceCorrector() throws InvocationTargetException {
		throw new InvocationTargetException(new IllegalAccessException(Messages.PRIVATE_CONS_MSG));
	}

	static class TextModel {
		final String first;
		final String last;
		final boolean isReverse;
		final int length;

		TextModel(String first, String last, boolean isReverse) {
			this.first = first == null ? "" : first;
			this.last = last == null ? "" : last;
			this.isReverse = isReverse;
			this.length = this.toString().length();
		}

		TextModel(String first, String last) {
			this(first, last, false);
		}

		@Override
		public String toString() {
			return String.join("-", this.first, this.last);
		}

		@Override
		public int hashCode() {
			return "MarkovChain.TextModel".hashCode() + String.join("-", this.first, this.last).hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof TextModel)) {
				return false;
			}
			final TextModel modelObj = (TextModel) obj;
			return this.first.equals(modelObj.first) && this.last.equals(modelObj.last);
		}
	}

	private static List<String> getTrainDataFromFile(final String trainingDataPath) throws IOException {
		String fileText = "";
		try (InputStream stream = SentenceCorrector.class.getClassLoader().getResourceAsStream(trainingDataPath)) {
			fileText = IOUtils.toString(stream, StandardCharsets.UTF_8).trim();
		}
		final String[] sentenceArray = fileText.toLowerCase().replace("\\r", "").replace("\\n", "").replace("?", ".")
				.replace(",", ".").replace("\\", " ").replace("/", " ").replace("!", ".").replace("“", ".")
				.replace("”", ".").replace("\"", ".").replace("‘", " ").replace("’", " ").replace("'", " ")
				.replace("-", " ").replace("’", " ").replace("\"", " ").replace(":", " ").split("\\.");

		return Stream.of(sentenceArray).map(elem -> elem.trim().split(" ")).flatMap(Arrays::stream)
				.collect(Collectors.toList());
	}

	/**
	 * Maps each words with the previous word and the next word. <br/>
	 * Eg: <br/>
	 * { .................... <br/>
	 * .. "am" : ............ <br/>
	 * ... { ................ <br/>
	 * ...... "I" - "am", ... <br/>
	 * ...... "am" - "I" .... <br/>
	 * ... } ................ <br/>
	 * } .................... <br/>
	 * 
	 * @param wordList
	 * @return
	 */
	private static Map<String, Map<TextModel, Integer>> getTextModelMap(List<String> wordList) {
		final Map<String, Map<TextModel, Integer>> mapWordWithTextModel = new HashMap<>();
		final int totalTextWords = wordList.size();
		for (int index = 0; index < totalTextWords - 1; index++) {
			final String currentWord = wordList.get(index);
			final Map<TextModel, Integer> mapOfTextModel = mapWordWithTextModel.computeIfAbsent(currentWord,
					map -> new HashMap<>());
			// Add TextModel
			final TextModel textModel = new TextModel(currentWord, wordList.get(index + 1));
			int count = mapOfTextModel.computeIfAbsent(textModel, initCount -> 0);
			mapOfTextModel.put(textModel, ++count);
			// Add TextModel - Reverse
			if (index > 0) {
				final TextModel textModelReverse = new TextModel(currentWord, wordList.get(index - 1), true);
				count = mapOfTextModel.computeIfAbsent(textModelReverse, initCount -> 0);
				mapOfTextModel.put(textModelReverse, ++count);
			}
		}
		return mapWordWithTextModel;
	}

	private static Entry<TextModel, Integer> getMostFeasibleTextModelEntry(
			final Map<String, Map<TextModel, Integer>> wordMappedWithTextModel, final String prePostWord,
			final String currentWord, final boolean isReverse, Entry<TextModel, Integer> feasibleTextModelEntry) {

		final Map<TextModel, Integer> textModelMappedWithCount = wordMappedWithTextModel.get(prePostWord);
		if (textModelMappedWithCount != null) {
			final int lenTextModelForPrev = new TextModel(currentWord, prePostWord).length;
			for (final Entry<TextModel, Integer> textModelEntry : textModelMappedWithCount.entrySet()) {
				final TextModel textModel = textModelEntry.getKey();
				final int count = textModelEntry.getValue();
				if (textModel.isReverse == isReverse && textModel.length == lenTextModelForPrev
						&& count > feasibleTextModelEntry.getValue()) {
					feasibleTextModelEntry = textModelEntry;
				}
			}
		}
		return feasibleTextModelEntry;
	}

	public static String[] getMostFeasibleSequence(final String[] words, final String trainingDataPath)
			throws IOException {

		final List<String> sentencesFromFile = SentenceCorrector.getTrainDataFromFile(trainingDataPath);
		final Map<String, Map<TextModel, Integer>> wordMappedWithTextModel = SentenceCorrector
				.getTextModelMap(sentencesFromFile);

		for (int i = 0; i < words.length - 1; i++) {
			final String currentWord = words[i];
			Map<TextModel, Integer> textModelMappedWithCount = wordMappedWithTextModel.get(currentWord);
			if (textModelMappedWithCount != null) {
				continue;
			}
			log.info("Using AI to identify the replacement word for '{}'.", currentWord);

			Map<TextModel, Integer> dummyTextModelEntryMap = new HashMap<>();
			dummyTextModelEntryMap.put(new TextModel("", ""), -1);
			Entry<TextModel, Integer> feasibleTextModelEntry = dummyTextModelEntryMap.entrySet().iterator().next();

			boolean isReverse = true;
			final String nextWord = words[i + 1];

			feasibleTextModelEntry = SentenceCorrector.getMostFeasibleTextModelEntry(wordMappedWithTextModel, nextWord,
					currentWord, isReverse, feasibleTextModelEntry);

			if (i > 0) {
				isReverse = false;
				final String prevWord = words[i - 1];
				feasibleTextModelEntry = SentenceCorrector.getMostFeasibleTextModelEntry(wordMappedWithTextModel, prevWord,
						currentWord, isReverse, feasibleTextModelEntry);
			}

			final TextModel feasibleTextModel = feasibleTextModelEntry.getKey();
			if (feasibleTextModel.equals(new TextModel("", ""))) {
				log.info("Model is not trained to identify the replacement word for '{}'!", currentWord);
			} else {
				log.info("Model is trained to identify the replacement word for '{}'!", currentWord);
				words[i] = feasibleTextModel.last;
			}
		}
		return words;
	}

	public static String[] getMostFeasibleSequence(final String[] words) throws IOException {
		return SentenceCorrector.getMostFeasibleSequence(words, "text/sample.txt");
	}

}
