package spring.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import net.sourceforge.tess4j.TesseractException;
import reader.ImageReader;

@Controller
public class FileUploadController {

	@RequestMapping("/")
	public String index() {
		return "upload.html";
	}

	@PostMapping(value = "/upload")
	public RedirectView singleFileUpload(@RequestParam("file") MultipartFile file,
			RedirectAttributes redirectAttributes, Model model) throws IOException, TesseractException {
		try {
			final byte[] bytes = file.getBytes();
			final Path path = Paths.get("backup//" + file.getOriginalFilename());
			Files.write(path, bytes);

			final File convertedFile = FileUploadController.convert(file);
			final String text = ImageReader.extractImage(convertedFile, "eng");
			redirectAttributes.addFlashAttribute("file", file);
			redirectAttributes.addFlashAttribute("text", text);
		} catch (Exception ex) {
			final String msg = "An exception occurred, unable to recognize the characters";
			final String text = String.format("%s: %s", msg, ex.getMessage());
			redirectAttributes.addFlashAttribute("text", text);
		}
		return new RedirectView("result.html");
	}

	@GetMapping("/result.html")
	public String result() {
		return "result.html";
	}

	public static File convert(MultipartFile file) throws IOException {
		final File convFile = new File(file.getOriginalFilename());
		// convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}
}