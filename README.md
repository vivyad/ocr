
# OCR Implementation

Project implementation performed using JavaTess.
JavaTess is Tesseract's OCR library written in C++ for recognizing character in the file with various formats.  

**The pipeline is not configured for TESSERACT_PREFIX environment variable, hence disabled  the pipeline execution on commit.** 

### Project Execution

#### Using Java Files

Execute implementation using [OcrDemo.java](src/main/java/reader/OcrDemo.java) or test using [OcrDemoTest.java](src/test/java/reader/OcrDemoTest.java)

Steps:

1. Input Image File [EnglishText_vague.png](src/main/resources/img/EnglishText_vague.png) is 
   scanned using Java Tesseract lib
    ![EnglishText_vague.png](src/main/resources/img/EnglishText_vague.png)
2. The scanned text is:
   ```
   | am curious about
   area-filling text
   rendering options
   ```
3. The output is corrected using data (words) from [sample.txt](src/main/resources/text/sample.txt), to find the most suitable word replacement.
4. Output is `[i, am, curious, about, area, filling, text, rendering, options]`
5. How? The [sample.txt](src/main/resources/text/sample.txt) contains many sentences with 'I am', therefore, 'I' stands out to be the best alternative with word length as one for the word '|'.


Word corrections performed by [SentenceCorrector.java](src/main/java/reader/SentenceCorrector.java) within [SentenceCorrectorTest.java](src/test/java/reader/SentenceCorrectorTest.java):

1. Using [sample.txt](src/main/resources/text/sample.txt) the input list of word `"then", "!!!!", "blah", "blue"` is corrected to `"then", "this", "fear", "blue"`
2. Using [sample_manipulated.txt](src/main/resources/text/sample_manipulated.txt) the input list of word `"then", "!!!!", "blah", "blue"` is corrected to `"then", "this", "talk", "blue"`

#### Spring Boot

The only Rest Service that is available right now is 'http://localhost:8080',
it accepts input image file and displays the text from the input file.
***Text correction will be implemented later.***


### Libraries

#### Tesseract

Download the master branch of **tessdata** repository folder from https://github.com/tesseract-ocr/tessdata.

If you are working on linux than and if you encounter the error mentioned below then please perform the steps mentioned below the error.

Error:
```
Exception in thread "main" java.lang.UnsatisfiedLinkError: Unable to load library 'tesseract':
libtesseract.so: cannot open shared object file: No such file or directory
libtesseract.so: cannot open shared object file: No such file or directory
Native library (linux-x86-64/libtesseract.so) not found in resource path 
    at com.sun.jna.NativeLibrary.loadLibrary(NativeLibrary.java:302)
    at com.sun.jna.NativeLibrary.getInstance(NativeLibrary.java:455)
    at com.sun.jna.Library$Handler.<init>(Library.java:192)
    at com.sun.jna.Native.loadLibrary(Native.java:646)
    at com.sun.jna.Native.loadLibrary(Native.java:630)
    at net.sourceforge.tess4j.util.LoadLibs.getTessAPIInstance(LoadLibs.java:85)
    at net.sourceforge.tess4j.TessAPI.<clinit>(TessAPI.java:42)
    at net.sourceforge.tess4j.Tesseract.init(Tesseract.java:427)
    at net.sourceforge.tess4j.Tesseract.doOCR(Tesseract.java:223)
    at net.sourceforge.tess4j.Tesseract.doOCR(Tesseract.java:195)
```


Resolution steps:
1. Open terminal
2. Execute code: `sudo apt-get install tesseract-ocr`
